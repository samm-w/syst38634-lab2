package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {
	
	@Test
	public void testgetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testgetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:0A");
		assertFalse("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test
	public void testgetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testgetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:05:1000");
		assertFalse("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);		
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:01a");
		assertFalse("The time provided does not match the result", totalSeconds == 3661);
		
	}

	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided does not match the result", totalSeconds == 3719);		
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
		
	}
	
	
}
